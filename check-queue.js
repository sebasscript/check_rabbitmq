const amqp = require("amqplib");
const {
  rabbitMQ: { host, port },
} = require("./config.js");

const rabbitHost = process.env?.RABBIT_MQ_HOST
  ? process.env.RABBIT_MQ_HOST
  : host;

async function main() {
  try {
    // !get queue name from config file
    const queueName = "metadata";
    connection = await amqp.connect(`amqp://${rabbitHost}:${port}`);
    channel = await connection.createChannel();
    connection.on("error", function (err) {
      console.log("Cant find queue, closing channel");
    });
    const loop = setInterval(async () => {
      try {
        const resp = await channel.checkQueue(queueName);
        console.log(resp);
      } catch {
        console.log("Lost connection, try to reconnect");
        clearInterval(loop);
        main();
      }
    }, 5000);
  } catch (err) {
    console.log("Cant find Queue in rabbitMq, Try again in 5 seconds");
    setTimeout(() => main(), 5000);
  }
}
main();
