FROM node:14-slim as build


WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm install \
    && npm install cache clean --force 

COPY . .

CMD ["node", "check-queue.js"]