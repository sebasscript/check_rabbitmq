module.exports = {
  frontend: {
    host: "localhost",
    port: 3000,
  },
  imageQuality: 90,
  viewPort: {
    height: 1080,
    width: 1080,
  },
  rabbitMQ: {
    user: "guest",
    pw: "guest",
    host: "localhost",
    port: 5672,
  },
  webglRender: "egl",
};
